0.1.1 2018-12-09
----------------

* Bumped minimum Quart version to 0.6.11 due to a bug in Quart.

0.1.0 2018-06-11
----------------

* Released initial alpha version.
